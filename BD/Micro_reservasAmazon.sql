-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de datos: Micro_reservas
--

CREATE OR REPLACE DATABASE Micro_reservas CHARSET=utf8 COLLATE=utf8_bin;

USE Micro_reservas;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tbl_usuario
--


CREATE TABLE tbl_usuario (
	id_usuario bigint(20) NOT NULL AUTO_INCREMENT,
	id_rol int(1) NOT NULL,
	identificacion varchar(30) NOT NULL,
	nombres varchar(100) NOT NULL,
	apellidos varchar(100) NOT NULL,
	correo_electronico varchar(150) NOT NULL,
	estado tinyint(1) NOT NULL,
	PRIMARY KEY(id_usuario)
);
ALTER TABLE tbl_usuario ADD INDEX(id_rol);

create table tbl_Reserva (
	id_reserva bigint(20) NOT NULL AUTO_INCREMENT,
	id_usuario bigint(20) NOT NULL,
	id_sala int(11) NOT NULL,
	fecha_hora_creacion timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	fechahora_inicio timestamp NULL DEFAULT NULL,
	fechahora_fin timestamp NULL DEFAULT NULL,
	confirmacion tinyint(1) NOT NULL,
	estado tinyint(1) NOT NULL,
	PRIMARY KEY(id_reserva)
);
ALTER TABLE tbl_Reserva ADD INDEX(id_usuario);
ALTER TABLE tbl_Reserva ADD INDEX(id_sala);
ALTER TABLE tbl_Reserva ADD INDEX(fechahora_inicio);
ALTER TABLE tbl_Reserva ADD INDEX(fechahora_fin);

Create table tbl_Sala (
	id_sala int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(100) NOT NULL,
	descripcion varchar(500),
	estado tinyint(1) NOT NULL,
	PRIMARY KEY(id_sala)
);


create table tbl_Rol (
	id_rol int(1) NOT NULL AUTO_INCREMENT,
	Rol varchar(20) NOT NULL,
	PRIMARY KEY(id_rol)
);

create table tbl_Encuesta (
	id_encuesta int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(50),
	descripcion varchar(200),
	estado tinyint(1) NOT NULL,
	PRIMARY KEY(id_encuesta)
);

create table tbl_respuesta (
	id_respuesta int(11) NOT NULL AUTO_INCREMENT,
	id_encuesta int(11) NOT NULL,
	id_pregunta int(11) NOT NULL,
	id_usuario bigint(20) NOT NULL,
	id_reserva bigint(20) NOT NULL,
	respuesta varchar(200) NOT NULL,
	PRIMARY KEY(id_respuesta)
);
ALTER TABLE tbl_respuesta ADD INDEX(id_encuesta);
ALTER TABLE tbl_respuesta ADD INDEX(id_pregunta);
ALTER TABLE tbl_respuesta ADD INDEX(id_usuario);
ALTER TABLE tbl_respuesta ADD INDEX(id_reserva);


create table tbl_Computo(
	id_computo int(11) NOT NULL AUTO_INCREMENT,
	id_sala int(11) NOT NULL,
	descripcion varchar(200),
	PRIMARY KEY(id_computo)
);
ALTER TABLE tbl_respuesta ADD INDEX(id_reserva);


create table tbl_Encuesta_pregunta(
	id_encuesta int(11) NOT NULL AUTO_INCREMENT,
	id_pregunta int(11) NOT NULL,
	PRIMARY KEY(id_encuesta)
);
ALTER TABLE tbl_Encuesta_pregunta ADD INDEX(id_pregunta);

create table tbl_pregunta(
	id_pregunta int(11) NOT NULL AUTO_INCREMENT,
	pregunta varchar(80),
	estado tinyint(1) NOT NULL,
	PRIMARY KEY(id_pregunta)
);

ALTER TABLE tbl_usuario ADD CONSTRAINT id_rol FOREIGN KEY (id_rol) REFERENCES tbl_rol(id_rol) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE tbl_Reserva ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES tbl_usuario(id_usuario) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE tbl_Reserva ADD CONSTRAINT id_sala FOREIGN KEY (id_sala) REFERENCES tbl_Sala(id_sala) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE tbl_respuesta ADD CONSTRAINT id_encuesta FOREIGN KEY (id_encuesta) REFERENCES tbl_encuesta(id_encuesta) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE tbl_respuesta ADD CONSTRAINT id_pregunta FOREIGN KEY (id_pregunta) REFERENCES tbl_pregunta(id_pregunta) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE tbl_respuesta ADD CONSTRAINT id_usuario_r FOREIGN KEY (id_usuario) REFERENCES tbl_usuario(id_usuario) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE tbl_respuesta ADD CONSTRAINT id_reserva FOREIGN KEY (id_reserva) REFERENCES tbl_reserva(id_reserva) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE tbl_Computo ADD CONSTRAINT id_sala_c FOREIGN KEY (id_sala) REFERENCES tbl_Sala(id_sala) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE tbl_Encuesta_pregunta ADD CONSTRAINT id_pregunta_ep FOREIGN KEY (id_pregunta) REFERENCES tbl_pregunta(id_pregunta) ON DELETE RESTRICT ON UPDATE RESTRICT;

COMMIT;