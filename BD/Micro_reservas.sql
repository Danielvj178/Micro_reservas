-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-01-2019 a las 06:33:15
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `micro_reservas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_agenda`
--

CREATE TABLE `tbl_agenda` (
  `id_agenda` bigint(20) NOT NULL,
  `id_sala` bigint(20) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `fecha_fin` date NOT NULL,
  `hora_fin` time NOT NULL,
  `hora_disponible` time NOT NULL,
  `duracion` varchar(10) COLLATE utf8_bin NOT NULL,
  `estado` smallint(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tbl_agenda`
--

INSERT INTO `tbl_agenda` (`id_agenda`, `id_sala`, `fecha_inicio`, `hora_inicio`, `fecha_fin`, `hora_fin`, `hora_disponible`, `duracion`, `estado`, `created_at`, `updated_at`) VALUES
(19584, 1, '2018-11-29', '07:00:00', '2018-11-29', '18:00:00', '09:00:00', '2', 0, '2018-11-29 07:18:53', '2018-11-29 07:18:53'),
(19585, 1, '2018-11-29', '07:00:00', '2018-11-29', '18:00:00', '11:00:00', '2', 0, '2018-11-29 07:18:53', '2018-11-29 07:18:53'),
(19586, 1, '2018-11-29', '07:00:00', '2018-11-29', '18:00:00', '13:00:00', '2', 0, '2018-11-29 07:18:53', '2018-11-29 07:18:53'),
(19587, 1, '2018-11-29', '07:00:00', '2018-11-29', '18:00:00', '15:00:00', '2', 0, '2018-11-29 07:18:53', '2018-11-29 07:18:53'),
(19588, 1, '2018-11-29', '07:00:00', '2018-11-29', '18:00:00', '17:00:00', '2', 0, '2018-11-29 07:18:53', '2018-11-29 07:18:53'),
(19589, 1, '2018-11-29', '07:00:00', '2018-11-29', '18:00:00', '19:00:00', '2', 0, '2018-11-29 07:18:53', '2018-11-29 07:18:53'),
(19590, 5, '2018-11-30', '19:00:00', '2018-11-30', '22:00:00', '20:00:00', '1', 0, '2018-11-29 07:19:39', '2018-11-29 07:19:39'),
(19591, 5, '2018-11-30', '19:00:00', '2018-11-30', '22:00:00', '21:00:00', '1', 0, '2018-11-29 07:19:39', '2018-11-29 07:19:39'),
(19592, 5, '2018-11-30', '19:00:00', '2018-11-30', '22:00:00', '22:00:00', '1', 0, '2018-11-29 07:19:39', '2018-11-29 07:19:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_computos`
--

CREATE TABLE `tbl_computos` (
  `id_computo` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_encuestas`
--

CREATE TABLE `tbl_encuestas` (
  `id_encuesta` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_encuesta_preguntas`
--

CREATE TABLE `tbl_encuesta_preguntas` (
  `id_encuesta` int(11) NOT NULL,
  `id_pregunta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_preguntas`
--

CREATE TABLE `tbl_preguntas` (
  `id_pregunta` int(11) NOT NULL,
  `pregunta` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_reservas`
--

CREATE TABLE `tbl_reservas` (
  `id_reserva` bigint(20) NOT NULL,
  `id_usuario` bigint(20) NOT NULL,
  `id_sala` int(11) NOT NULL,
  `id_agenda` int(11) NOT NULL,
  `confirmacion` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tbl_reservas`
--

INSERT INTO `tbl_reservas` (`id_reserva`, `id_usuario`, `id_sala`, `id_agenda`, `confirmacion`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 1, '2018-11-13 09:43:31', '2018-11-13 09:43:31'),
(2, 1, 1, 0, 1, '2018-11-18 08:57:35', '2018-11-18 08:57:35'),
(3, 1, 1, 19586, 0, '2019-01-02 10:18:29', '2019-01-02 10:18:29'),
(4, 1, 1, 19587, 0, '2019-01-02 10:18:29', '2019-01-02 10:18:29'),
(5, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(6, 1, 1, 19587, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(7, 1, 1, 19591, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(8, 1, 1, 19591, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(9, 1, 1, 19591, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(10, 1, 1, 19591, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(11, 1, 1, 19592, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(12, 1, 1, 19592, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(13, 1, 1, 19591, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(14, 1, 1, 19591, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(15, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(16, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(17, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(18, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(19, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(20, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(21, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(22, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(23, 1, 1, 19586, 0, '2019-01-02 10:20:54', '2019-01-02 10:20:54'),
(24, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(25, 1, 1, 19587, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(26, 1, 1, 19591, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(27, 1, 1, 19591, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(28, 1, 1, 19591, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(29, 1, 1, 19591, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(30, 1, 1, 19592, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(31, 1, 1, 19592, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(32, 1, 1, 19591, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(33, 1, 1, 19591, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(34, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(35, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(36, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(37, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(38, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(39, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(40, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(41, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12'),
(42, 1, 1, 19586, 0, '2019-01-02 10:21:12', '2019-01-02 10:21:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_respuestas`
--

CREATE TABLE `tbl_respuestas` (
  `id_respuesta` int(11) NOT NULL,
  `id_encuesta` int(11) NOT NULL,
  `id_pregunta` int(11) NOT NULL,
  `id_usuario` bigint(20) NOT NULL,
  `id_reserva` bigint(20) NOT NULL,
  `respuesta` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_rols`
--

CREATE TABLE `tbl_rols` (
  `id_rol` int(1) NOT NULL,
  `Rol` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_salas`
--

CREATE TABLE `tbl_salas` (
  `id_sala` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tbl_salas`
--

INSERT INTO `tbl_salas` (`id_sala`, `nombre`, `descripcion`, `estado`, `updated_at`, `created_at`) VALUES
(1, 'Daniel', '12', 1, '2018-11-23 10:14:12', '2018-11-23 10:14:12'),
(2, 'Daniel', '12', 1, '2018-11-23 10:14:46', '2018-11-23 10:14:46'),
(3, 'Sala 309', 'Bonita sala', 1, '2018-11-26 05:30:01', '2018-11-26 05:30:01'),
(4, 'Sala 309', 'Bonita sala', 1, '2018-11-26 05:33:24', '2018-11-26 05:33:24'),
(5, 'Sala 309', 'Bonita sala', 1, '2018-11-26 05:35:13', '2018-11-26 05:35:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

CREATE TABLE `tbl_usuarios` (
  `id_usuario` bigint(20) NOT NULL,
  `id_rol` int(1) NOT NULL,
  `identificacion` varchar(30) COLLATE utf8_bin NOT NULL,
  `nombres` varchar(100) COLLATE utf8_bin NOT NULL,
  `apellidos` varchar(100) COLLATE utf8_bin NOT NULL,
  `correo_electronico` varchar(150) COLLATE utf8_bin NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id_usuario`, `id_rol`, `identificacion`, `nombres`, `apellidos`, `correo_electronico`, `estado`) VALUES
(1, 1, '123', 'Daniel', 'Vidal', 'vidal178@hotmail.com', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_agenda`
--
ALTER TABLE `tbl_agenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indices de la tabla `tbl_computos`
--
ALTER TABLE `tbl_computos`
  ADD PRIMARY KEY (`id_computo`);

--
-- Indices de la tabla `tbl_encuestas`
--
ALTER TABLE `tbl_encuestas`
  ADD PRIMARY KEY (`id_encuesta`);

--
-- Indices de la tabla `tbl_encuesta_preguntas`
--
ALTER TABLE `tbl_encuesta_preguntas`
  ADD PRIMARY KEY (`id_encuesta`);

--
-- Indices de la tabla `tbl_preguntas`
--
ALTER TABLE `tbl_preguntas`
  ADD PRIMARY KEY (`id_pregunta`);

--
-- Indices de la tabla `tbl_reservas`
--
ALTER TABLE `tbl_reservas`
  ADD PRIMARY KEY (`id_reserva`);

--
-- Indices de la tabla `tbl_respuestas`
--
ALTER TABLE `tbl_respuestas`
  ADD PRIMARY KEY (`id_respuesta`);

--
-- Indices de la tabla `tbl_rols`
--
ALTER TABLE `tbl_rols`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `tbl_salas`
--
ALTER TABLE `tbl_salas`
  ADD PRIMARY KEY (`id_sala`);

--
-- Indices de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_agenda`
--
ALTER TABLE `tbl_agenda`
  MODIFY `id_agenda` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19593;

--
-- AUTO_INCREMENT de la tabla `tbl_computos`
--
ALTER TABLE `tbl_computos`
  MODIFY `id_computo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_encuestas`
--
ALTER TABLE `tbl_encuestas`
  MODIFY `id_encuesta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_encuesta_preguntas`
--
ALTER TABLE `tbl_encuesta_preguntas`
  MODIFY `id_encuesta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_preguntas`
--
ALTER TABLE `tbl_preguntas`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_reservas`
--
ALTER TABLE `tbl_reservas`
  MODIFY `id_reserva` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `tbl_respuestas`
--
ALTER TABLE `tbl_respuestas`
  MODIFY `id_respuesta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_rols`
--
ALTER TABLE `tbl_rols`
  MODIFY `id_rol` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_salas`
--
ALTER TABLE `tbl_salas`
  MODIFY `id_sala` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  MODIFY `id_usuario` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
