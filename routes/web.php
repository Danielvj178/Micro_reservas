<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Ruta original
// Route::get('/', function () {
//     return view('welcome');
// });

// Ruta principal
Route::get('/', function () {
    return view('front.login');
});

// Definición de rutas de la aplicación
Route::resource('login', 'LoginController');
Route::resource('inicio', 'InicioController');
Route::resource('confirmacion', 'ConfirmacionController');
Route::resource('equipos', 'EquiposController');
Route::resource('salas', 'SalasController');
Route::resource('salasCreadas', 'SalasController');
Route::resource('dashboard', 'DashboardController');
Route::resource('informes', 'InformesController');
Route::resource('agenda','AgendaController');
Route::resource('reserva_sala','ReservaController');


// Definición de rutas para Ajax
Route::post('consultarSalas', 'SalasController@consultarSalas');
Route::post('consultarSalasHor', 'AgendaController@consultarSalasHor');
Route::post('consultarHorarios', 'AgendaController@consultarHorarios');