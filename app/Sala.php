<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_sala
 * @property string $nombre
 * @property string $descripcion
 * @property boolean $estado
 * @property string $updated_at
 * @property string $created_at
 */
class Sala extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_salas';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_sala';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion', 'estado', 'updated_at', 'created_at'];

}
