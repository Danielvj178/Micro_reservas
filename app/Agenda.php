<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_agenda
 * @property integer $id_sala
 * @property string $fecha_inicio
 * @property string $hora_inicio
 * @property string $fecha_fin
 * @property string $hora_fin
 * @property string $hora_disponible
 * @property string $duracion
 * @property integer $estado
 * @property string $created_at
 * @property string $updated_at
 */
class Agenda extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_agenda';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_agenda';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_sala', 'fecha_inicio', 'hora_inicio', 'fecha_fin', 'hora_fin', 'hora_disponible', 'duracion', 'estado', 'created_at', 'updated_at'];

}
