<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_pregunta
 * @property string $pregunta
 * @property boolean $estado
 */
class Pregunta extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_preguntas';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_pregunta';

    /**
     * @var array
     */
    protected $fillable = ['pregunta', 'estado'];

}
