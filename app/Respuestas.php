<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_respuesta
 * @property int $id_encuesta
 * @property int $id_pregunta
 * @property integer $id_usuario
 * @property integer $id_reserva
 * @property string $respuesta
 */
class Respuestas extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_respuestas';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_respuesta';

    /**
     * @var array
     */
    protected $fillable = ['id_encuesta', 'id_pregunta', 'id_usuario', 'id_reserva', 'respuesta'];

}
