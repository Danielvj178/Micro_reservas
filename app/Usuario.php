<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_usuario
 * @property int $id_rol
 * @property string $identificacion
 * @property string $nombres
 * @property string $apellidos
 * @property string $correo_electronico
 * @property boolean $estado
 */
class Usuario extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_usuarios';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_usuario';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_rol', 'identificacion', 'nombres', 'apellidos', 'correo_electronico', 'estado'];

}
