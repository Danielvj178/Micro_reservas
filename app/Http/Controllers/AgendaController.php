<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agenda;
use DB;
use Session;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.agenda');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($hola);
        //$this->validaciones($request);

        $rules = [
            'id_sala' => 'required',
            /*'fecha_inicio' => 'required',
            'hora_inicio' => 'required',
            'fecha_fin' => 'required',
            'hora_fin' => 'required',
            'duracion' => 'required'*/
        ];

        $messages = [
            'id_sala.required' => 'Por favor seleccione la sala',
            /*'fecha_inicio.required' => 'Por favor seleccione la fecha de inicio',
            'hora_inicio.required' => 'Por favor seleccione la hora de inicio',
            'fecha_fin.required' => 'Por favor seleccione la fecha de fin',
            'hora_fin.required' => 'Por favor seleccione la hora de fin',
            'duracion.required' => 'Por favor seleccione la duración',*/
        ];
         
        //$this->validate($request, $rules, $messages);

        // dd($request->input('sala'));
        $band = true;
        $agenda = new Agenda();
        $horaInicio = strtotime($request->input('horaIni'));
        $agenda->hora_inicio = date("H:i", $horaInicio);
        $horaFin = strtotime($request->input('horaFin'));
        $agenda->hora_fin = date("H:i", $horaFin);

        $horaInicio = $agenda->hora_inicio;

        while ($horaInicio < $agenda->hora_fin) {
            $agenda->id_sala = $request->input('sala');
            $agenda->duracion = $request->input('duracion');
            $agenda->fecha_inicio = $request->input('fechaIni');
            $agenda->fecha_fin = $request->input('fechaFin');
            $agenda->hora_disponible =  date('H:i', strtotime($horaInicio.'+ '.$agenda->duracion.' hours'));
            $agenda->estado = 1;

            ( $agenda->save() ) ? $band = true : $band = false; 
            $horaInicioOrig = $agenda->hora_inicio;
            $horaDisponible = $agenda->hora_disponible;
            $horaFin = $agenda->hora_fin;
            $agenda = new Agenda();
            $horaInicio = $horaDisponible;
            $agenda->hora_fin = $horaFin;
            $agenda->hora_inicio = $horaInicioOrig;
        }

        if( $band )
            Session::flash('flash_message', 'Agenda creada exitosamente!');

        return view('front.agenda');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*
        Función para consulta de salas por fechas
    */
    public function consultarSalasHor(){
        $fechaInicio = $_POST['fecha_inicio'];
        $fechaFin = $_POST['fecha_fin'];

        $salas = DB::table('tbl_agenda')
        ->join('tbl_salas', 'tbl_agenda.id_sala', '=', 'tbl_salas.id_sala')
        ->select('tbl_salas.nombre', 'tbl_agenda.id_sala')
        ->whereBetween('fecha_inicio', [$fechaInicio, $fechaFin])
        ->whereBetween('fecha_fin', [$fechaInicio, $fechaFin])
        ->groupBy('tbl_salas.nombre','tbl_agenda.id_sala')
        ->get();

        echo json_encode($salas);
    }

    /* 
        Función para consultar los horarios por cada sala
    */
    public function consultarHorarios(){
        $sala = $_POST['sala'];
        $agenda = Agenda::whereRaw('id_sala = ?', array($sala))
        ->select('tbl_agenda.hora_disponible', 'tbl_agenda.id_agenda')
        ->get();

        echo json_encode($agenda);
    }

    /* Función para validaciones de formulario */
    public function validaciones($request){
        $rules = [
            'id_sala' => 'required',
            'fecha_inicio' => 'required',
            'hora_inicio' => 'required',
            'fecha_fin' => 'required',
            'hora_fin' => 'required',
            'duracion' => 'required'
        ];

        $messages = [
            'id_sala.required' => 'Por favor seleccione la sala',
            'fecha_inicio.required' => 'Por favor seleccione la fecha de inicio',
            'hora_inicio.required' => 'Por favor seleccione la hora de inicio',
            'fecha_fin.required' => 'Por favor seleccione la fecha de fin',
            'hora_fin.required' => 'Por favor seleccione la hora de fin',
            'duracion.required' => 'Por favor seleccione la duración',
        ];
         
        $this->validate($request, $rules, $messages);
    }
}
