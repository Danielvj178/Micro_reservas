<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agenda;
use App\Reserva;
use Session;

class ReservaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.reserva');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sala = $request->input('sala');
        $horarios = explode(";", $request->input('horario'));
        dd($sala);

        foreach ($horarios as $key => $value) {
            $reserva = new Reserva;
            $reserva->id_usuario = 1;
            $reserva->id_sala = $sala;
            $reserva->id_agenda = $value;
            $reserva->confirmacion = 0;

            $reserva->save();
        }

        Session::flash('flash_message', 'La reserva se ha realizado con éxito!');

        return view('front.reserva');
        /*$agenda = Agenda::whereRaw('id_sala = ?', array($sala))
        ->select('tbl_agenda.hora_disponible', 'tbl_agenda.id_agenda')
        ->get();*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
