<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reserva;
use App\Sala;
use Session;

class SalasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('front.salas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( $request->ajax() ){
            $salas = Sala::all();
            
            return Response()->json(
                $salas->toArray()
            );
        }

        $nombreSala = $request->input('nombre');
        $descripcionSala = $request->input('descripcion');
        $estado = $request->input('estado');

        $rules = [
            'nombre' => 'required',
            'estado'    => 'required'
        ];

        $messages = [
            'nombre.required' => 'Por favor ingrese el nombre de la sala',
            'estado.required' => 'Por favor seleccione el estado de la sala',
        ];
         
        $this->validate($request, $rules, $messages);

        $sala = new Sala();
        $sala->nombre = $nombreSala;
        $sala->descripcion = $descripcionSala;
        $sala->estado = $estado;

        ( $sala->save() ) ? Session::flash('flash_message', 'Sala creada exitosamente!') : Session::flash('flash_message', 'La sala no ha podido ser creada');

        return view('front.salas') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*
        Función para consulta de salas
    */
    public function consultarSalas(){
        $salas = Sala::all();

        echo json_encode($salas);
    }
}
