<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_reserva
 * @property integer $id_usuario
 * @property int $id_sala
 * @property int $id_agenda
 * @property boolean $confirmacion
 * @property string $created_at
 * @property string $updated_at
 */
class Reserva extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_reservas';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_reserva';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_usuario', 'id_sala', 'id_agenda', 'confirmacion', 'created_at', 'updated_at'];

}
