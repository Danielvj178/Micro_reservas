<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_encuesta
 * @property int $id_pregunta
 */
class Encuesta_preguntas extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_encuesta_preguntas';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_encuesta';

    /**
     * @var array
     */
    protected $fillable = ['id_pregunta'];

}
