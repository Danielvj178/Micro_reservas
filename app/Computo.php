<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_computo
 * @property int $id_sala
 * @property string $descripcion
 */
class Computo extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_computos';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_computo';

    /**
     * @var array
     */
    protected $fillable = ['id_sala', 'descripcion'];

}
