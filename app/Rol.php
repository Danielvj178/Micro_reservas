<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_rol
 * @property string $Rol
 */
class Rol extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_rols';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_rol';

    /**
     * @var array
     */
    protected $fillable = ['Rol'];

}
