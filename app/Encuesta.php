<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_encuesta
 * @property string $nombre
 * @property string $descripcion
 * @property boolean $estado
 */
class Encuesta extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_encuestas';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_encuesta';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion', 'estado'];

}
