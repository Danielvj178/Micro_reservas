@extends('layouts.app')

@section('title', 'Portal de reservas de Micros')

@section('content')
    
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Administración de salas</a>
      </li>
      <li class="breadcrumb-item active">Reserva de salas</li>
    </ol>

    <form method="POST" action="reserva_sala">
        @csrf
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <input type="hidden" name="horario" id="horarioSeleccionado">
        <div class="alert alert-success" style="text-align: center; margin: 0 100px 0 100px">
            Reserva de salas
        </div>
        <hr>
        {{-- Mensaje para validaciones --}}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <p>Corrige los siguientes errores:</p>
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{-- Mensaje de estado de la acción --}}
        @if(Session::has('flash_message'))
          <div class="alert alert-success" style="text-align: center;">{{Session::get('flash_message')}}</div>
        @endif

        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">1</div>
          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">2</div>
          <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">3</div>
        </div>

        <div class="content form-group">
        	<div class="row justify-content-md-center">
        		<div class="col col-md-2">
        			<label>Fecha inicio</label>
        		</div>
        		<div class="col col-md-3">
        			<input type="text" name="fechaIni" id="fechaIni" class="form-control" />
        		</div>
        		<div class="col col-md-2">
        			<label>Fecha fin</label>
        		</div>
        		<div class="col col-md-3">
        			<input type="text" name="fechaFin" id="fechaFin"  class="form-control" />
        		</div>
        	</div>
        	<br>
        	<div class="row justify-content-md-center">
        		<button id="btnBuscar" class="btn btn-success">Buscar</button>
        	</div>
        	<hr>
        	<br>
        	<div class="row justify-content-md-center" id="divSala" style="display: none;">
               <div class="col col-md-2">
                  <label>Sala</label>
               </div>
               <div class="col-md-3">
                   <select id="selectSala" class="form-control" name="sala">
                       <option value="">Seleccione...</option>
                       <option value="9">Sala 302</option>
                   </select>
               </div>
           </div>
           <hr>
           <br>
           <div class="row justify-content-md-center" id="divHorarios">
           </div>
           <br>
           <div style="text-align: center;">
           		<button type="submit" class="btn btn-success">Reservar</button>
           </div>
        </div>
    </form>
@endsection

@section('scripts')
	<script type="text/javascript">


		$(document).ready(function (){
			token = $("#token").val();
			$("#btnBuscar").click(function (e){
        // url = "http://localhost/Micro_reservas/public/consultarSalasHor";
				url = "http://18.224.56.108/consultarSalasHor";
				fechaIni = $("#fechaIni").val();
				fechaFin = $("#fechaFin").val();
			    $("#selectSala").empty();
			    $("#selectSala").html();
			    $("#divSala").show(200);
				e.preventDefault();
			    $.ajax({
			        url: url,
			        type: 'POST',
			        headers: {'X-CSRF-TOKEN': token},
			        dataType: 'json',
			        data:{
			        	fecha_inicio: fechaIni,
			        	fecha_fin: fechaFin
			        },
			        success: function(result){
			        	$("#selectSala").append('<option value>Seleccione...</option>');
			            $(result).each(function (i, v){
                            $("#selectSala").append('<option value="'+v.id_sala+'">'+v.nombre+'</option>');
                        });
			        }
			    });
			});

			$("#selectSala").change(function (){
				sala = $(this).val();
        url = "http://18.224.56.108/consultarHorarios"
				// url = "http://localhost/Micro_reservas/public/consultarHorarios";
				$("#divHorarios").empty();
				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					headers: {'X-CSRF-TOKEN':token},
					data:{
						sala: sala
					},
					success: function (result){
						$(result).each(function (i, v){
							$("<div class='col col-md-2'><button class='btn btn-success btnHorario' id="+v.id_agenda+" value="+v.id_agenda+">"+v.hora_disponible+"</button></div>").appendTo("#divHorarios");
						});
					}
				});

				$(document).on("click", '.btnHorario',  function (e){
					e.preventDefault();

					if( $(this).hasClass('btn-primary') ) {
						$(this).removeClass('btn-primary');
						$(this).addClass('btn-success');
					} else {
						$(this).removeClass('btn-success');
						$(this).addClass('btn-primary');
					} 
					( $("#horarioSeleccionado").val().trim() != '' ) ? $("#horarioSeleccionado").val($("#horarioSeleccionado").val() + ";" +$(this).attr('id')) : $("#horarioSeleccionado").val($(this).attr('id'));
				});
				
			});
		});
	</script>
@endsection

