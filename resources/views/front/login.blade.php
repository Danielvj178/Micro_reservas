@extends('layouts.app-login')

@section('title', 'Portal de reservas de Micros')

@section('content')
    <div class="container">
	   <div class="card card-login mx-auto mt-5 ">
        <div class="card-header" style="background-color: #1c734c;color: white;">Reserva de Micros</div>
        <div class="card-body">
          <form method="POST" action="inicio">
            <!-- Protección de ataques peligrosos -->
			@csrf

      {{-- Mensaje de estado de la acción --}}
        @if(Session::has('flash_message'))
          <div class="alert alert-danger" style="text-align: center;">{{Session::get('flash_message')}}</div>
        @endif
			<div class="form-group">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Correo electrónico" required="required" autofocus="autofocus">
                <label for="inputEmail">Correo electrónico</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="password" id="inputPassword"  class="form-control" name="password" placeholder="Contraseña" required="required">
                <label for="inputPassword">Contraseña</label>
              </div>
            </div>
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="remember-me">
                  Recordar contraseña
                </label>
              </div>
            </div>
			<button type="submit" class="btn btn-personalizado btn-block">Ingresar</button>
            <!-- <a class="btn btn-primary btn-block" style="background-color: #196844; border-color: black" href="a.html">Ingresar</a> -->
          </form>
          <!-- <div class="text-center">
            <a class="d-block small mt-3" href="register.html">Register an Account</a>
            <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
          </div> -->
        </div>
        <script type="text/javascript">
            $("body").addClass('bg-dark');
        </script>
      </div>
    </div>
      	
@endsection