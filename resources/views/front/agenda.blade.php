@extends('layouts.app')

@section('title', 'Portal de reservas de Micros')

@section('content')
    
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Administración de salas</a>
      </li>
      <li class="breadcrumb-item active">Creación de agendas</li>
    </ol>
    
    <form method="POST" action="agenda">
        @csrf
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <div class="alert alert-success" style="text-align: center; margin: 0 100px 0 100px">
            Creación de agenda
        </div>
        <hr>
        {{-- Mensaje para validaciones --}}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <p>Corrige los siguientes errores:</p>
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{-- Mensaje de estado de la acción --}}
        @if(Session::has('flash_message'))
          <div class="alert alert-success" style="text-align: center;">{{Session::get('flash_message')}}</div>
        @endif
        <div class="form-group">
            <div class="row justify-content-md-center">
                <div class="col col-md-2">
                   <label>Sala</label>
                </div>
                <div class="col-md-3">
                    <select id="selectSala" class="form-control" name="sala">
                        <option value="">Seleccione...</option>
                    </select>
                </div>
                <div class="col col-md-2">
                    <label>Duración</label>
                </div>
                <div class="col col-md-3">
                    <select id="selectDur" class="form-control" name="duracion">
                        <option value="">Seleccione</option>
                        <option value="1">1 Hora</option>
                        <option value="2">2 Horas</option>
                    </select>
                </div>
            </div>
            <br>
            <div class="row justify-content-md-center">
                <div class="col col-md-2">
                    <label>Fecha inicio</label>
                </div>
                <div class="col col-md-3">
                    <input type="text" readonly="" name="fechaIni" class="form-control datepicker" />
                </div>
                <div class="col col-md-2">
                    <label>Hora inicio</label>
                </div>
                <div class="col col-md-3">
                    <input type="text" name="horaIni" class="form-control time_element"/>
                </div>
            </div>
            <br>
            <div class="row justify-content-md-center">
                <div class="col col-md-2">
                    <label>Fecha fin</label>
                </div>
                <div class="col col-md-3">
                    <input type="text" readonly="" name="fechaFin" class="form-control datepicker" />
                </div>
                <div class="col col-md-2">
                    <label>Hora fin</label>
                </div>
                <div class="col col-md-3">
                    <input type="text" name="horaFin" class="form-control time_element" />
                </div>
            </div>
            <br>
            <div style="text-align: center;">
                <button type="submit" class="btn btn-success">Crear agenda</button>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script>
        $(document).ready(function (){
            $('.datepicker').datepicker({
                dateFormat: 'yy-mm-dd',
            });
            $(".time_element").timepicki();
            $('.datepicker').datepicker("option","showAnim", 'slideDown');

            // url = "http://localhost/Micro_reservas/public/consultarSalas";
            url = "http://18.224.56.108/consultarSalas";
            token = $("#token").val();
            $.ajax({
                url: url,
                type: 'POST',
                headers: {'X-CSRF-TOKEN': token},
                dataType: 'json',
                success: function(result){
                    $(result).each(function (i, v){
                        $("#selectSala").append('<option value="'+v.id_sala+'">'+v.nombre+'</option>');
                    });
                }
            });
        });

        function Eliminar(btn){
            token = $("#token").val();
            fechaInicio = $("#fecha_inicio").val();
            fechaFin = $("#fecha_fin").val();
            url = "http://localhost/Micro_reservas/public/salasCreadas"

            $.ajax({
                url: url,
                type: 'DELETE',
                headers: {'X-CSRF-TOKEN': token},
                dataType: 'json',
                success: function(result){
                    $("#btnConsulta").click();
                }
            });
        }
    </script>
@endsection