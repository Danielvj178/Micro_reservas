@extends('layouts.app')

@section('title', 'Portal de reservas de Micros')

@section('content')
  <!-- Page Content -->
    <h2 style="text-align: center;">Gestión de salas</h2>
    <hr>
    <form class="form-group" id="form-salas" method="POST" action="salas" >
        @csrf
    <div class="form-group">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Creación de salas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Consulta de salas creadas</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <fieldset>
                <div class="container" style="margin-top: 20px">
                    {{-- Mensaje para validaciones --}}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <p>Corrige los siguientes errores:</p>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{-- Mensaje de estado de la acción --}}
                    @if(Session::has('flash_message'))
                      <div class="alert alert-success" style="text-align: center;">{{Session::get('flash_message')}}</div>
                    @endif

                    <div class="row justify-content-md-center">
                        <div class="col col-md-3">
                           <label>Nombre de la sala</label>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" name="nombre" placeholder="Ingrese nombre de la sala" />
                        </div>
                    </div>
                    <br>

                    <div class="row justify-content-md-center">
                        <div class="col-md-3">
                            <label>Descripción de la sala</label>
                        </div>
                        <div class="col-md-3">
                            <textarea name="descripcion" class="form-control"></textarea>
                        </div>
                    </div>
                    <br>

                    <div class="row justify-content-md-center form-group">
                        <div class="col-md-3">
                            <label for="estado" name="estado">Estado</label>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control" id="estado" name="estado">
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div style="text-align: center;">
                    <button type="submit" class="btn btn-success" >Crear sala</button>
                </div>
            </fieldset>
          </div>
          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="container" style="margin-top: 20px">
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <br>
                <div id="listSalas" class="row" style="display: none;">
                    <table class="table table-dark">
                        <thead>
                            <th style="text-align: center;">Sala</th>
                            <th style="text-align: center;">Acciones</th>
                        </thead>
                        <tbody id="datos" style="text-align: center;"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </form>
        
@endsection

@section('scripts')
    <script>
        $(document).ready(function (){
            $("#listSalas").show(200);
            $("#datos").empty();
            $("#datos").html();
            url = "http://localhost/Micro_reservas/public/salasCreadas"
            token = $("#token").val();
            $.ajax({
                url: url,
                type: 'POST',
                headers: {'X-CSRF-TOKEN': token},
                dataType: 'json',
                success: function(result){
                    $(result).each(function (i, v){
                        $("#datos").append("<tr><td>"+v.nombre+"</td><td><button class='btn btn-danger' value='"+v.id_sala+"' OnClick='Eliminar()'>Eliminar</button></td></tr>");
                    });
                }
            });
        });

        function Eliminar(btn){
            token = $("#token").val();
            fechaInicio = $("#fecha_inicio").val();
            fechaFin = $("#fecha_fin").val();
            url = "http://localhost/Micro_reservas/public/salasCreadas"

            $.ajax({
                url: url,
                type: 'DELETE',
                headers: {'X-CSRF-TOKEN': token},
                dataType: 'json',
                success: function(result){
                    $("#btnConsulta").click();
                }
            });
        }
    </script>
@endsection