@extends('layouts.app')

@section('title', 'Portal de reservas de Micros')

@section('content')
    <nav class="navbar navbar-expand navbar-dark static-top" style="background-color: #1c734c">

      <a class="navbar-brand mr-1" href="inicio">Portal de reservas de Micros</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <!-- <input type="text" class="form-control" placeholder="Buscar..." aria-label="Search" aria-describedby="basic-addon2"> -->
          <h6 style="color: white;">Daniel Vidal Jaramillo</h6>
          <!-- <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search"></i>
            </button>
          </div> -->
        </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <span class="badge badge-danger">9+</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
            <a class="dropdown-item" href="#">Ver notificaciones</a>
            <!-- <a class="dropdown-item" href="#">Another action</a> -->
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Ayuda</a>
          </div>
        </li>
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope fa-fw"></i>
            <span class="badge badge-danger">7</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
            <a class="dropdown-item" href="#">Ver solicitudes</a>
            <!-- <a class="dropdown-item" href="#">Another action</a> -->
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Ayuda</a>
          </div>
        </li>
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <!-- <a class="dropdown-item" href="#">Settings</a>
            <a class="dropdown-item" href="#">Activity Log</a> -->
            <!-- <div class="dropdown-divider"></div> -->
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Cerrar sesión</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="salas">
            <i class="fas fa-fw fa-table"></i>
            <span>Reservas de salas</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="equipos">
            <i class="fas fa-fw fa-table"></i>
            <span>Reservas de equipos</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="confirmacion">
            <i class="fas fa-fw fa-table"></i>
            <span>Confirmación de salas</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="informes">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Generación de informes</span></a>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="inicio">Confirmación</a>
            </li>
            <li class="breadcrumb-item active">Uso de salas</li>
          </ol>

          <!-- Page Content -->
          <h1 style="text-align: center;">Confirmación de reservas</h1>
          <hr>
          <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Área</th>
                      <th>Correo electrónico</th>
                      <th>Asignatura</th>
                      <th>Documento</th>
                      <th>Confirmación</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nombre</th>
                      <th>Facultad</th>
                      <th>Correo electrónico</th>
                      <th>Asignatura</th>
                      <th>Documento</th>
                      <th>Confirmación</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <tr>
                      <td>Maria</td>
                      <td>Ingeniería</td>
                      <td>maria@elpoli.edu.co</td>
                      <td>Fundamentos de programación</td>
                      <td><input type="text" name="txtDocumento" placeholder="Ingrese Documento" class="form-control form-control-sm"></td>
                      <td><button class="btn btn-primary">Confirmar</button></td>
                    </tr>
                    <tr>
                      <td>Milton</td>
                      <td>Ciencias básicas</td>
                      <td>milton@elpoli.edu.co</td>
                      <td>Estadística</td>
                      <td><input type="text" name="txtDocumento" placeholder="Ingrese Documento" class="form-control form-control-sm"></td>
                      <td><button class="btn btn-primary">Confirmar</button></td>
                    </tr>
                    <tr>
                      <td>Carlos</td>
                      <td>Ingeniería</td>
                      <td>carlos@elpoli.edu.co</td>
                      <td>Taller de programación</td>
                      <td><input type="text" name="txtDocumento" placeholder="Ingrese Documento" class="form-control form-control-sm"></td>
                      <td><button class="btn btn-primary">Confirmar</button></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- <p>This is a great starting point for new custom pages.</p> -->

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © Portal de reservas de Micros</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
        
@endsection