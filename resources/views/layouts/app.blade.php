<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title></title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/datepicker.css" rel="stylesheet">
		<link href="css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="css/timepicki.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

		<script src="js/jquery-easing/jquery.easing.min.js"></script>
		<script src="js/jquery/timepicki.js"></script>

		<script src="js/sb-admin.min.js"></script>

		<!-- Custom styles for this template-->
		<link href="css/sb-admin.css" rel="stylesheet">

		<title>Micro reservas - @yield('title')</title>
	</head>
	<body>
		<nav class="navbar navbar-expand navbar-dark static-top" style="background-color: #1c734c">

		  <a class="navbar-brand mr-1" href="inicio">Portal de reservas de Micros</a>

		  <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
		    <i class="fas fa-bars"></i>
		  </button>

		  <!-- Navbar Search -->
		  <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
		    <div class="input-group">
		      <h6 style="color: white;">Bienvenido Daniel Vidal Jaramillo</h6>
		    </div>
		  </form>

		  <!-- Navbar -->
		  <ul class="navbar-nav ml-auto ml-md-0">
		    <li class="nav-item dropdown no-arrow mx-1">
		      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		        <i class="fas fa-bell fa-fw"></i>
		        <span class="badge badge-danger">9+</span>
		      </a>
		      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
		        <a class="dropdown-item" href="#">Ver notificaciones</a>
		        <!-- <a class="dropdown-item" href="#">Another action</a> -->
		        <div class="dropdown-divider"></div>
		        <a class="dropdown-item" href="#">Ayuda</a>
		      </div>
		    </li>
		    <li class="nav-item dropdown no-arrow mx-1">
		      <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		        <i class="fas fa-envelope fa-fw"></i>
		        <span class="badge badge-danger">7</span>
		      </a>
		      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
		        <a class="dropdown-item" href="#">Ver solicitudes</a>
		        <!-- <a class="dropdown-item" href="#">Another action</a> -->
		        <div class="dropdown-divider"></div>
		        <a class="dropdown-item" href="#">Ayuda</a>
		      </div>
		    </li>
		    <li class="nav-item dropdown no-arrow">
		      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		        <i class="fas fa-user-circle fa-fw"></i>
		      </a>
		      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
		        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Cerrar sesión</a>
		      </div>
		    </li>
		  </ul>

		</nav>

		<div id="wrapper">

		  <!-- Sidebar -->
		  <ul class="sidebar navbar-nav">
		    <li class="nav-item">
		      <a class="nav-link" href="#">
		        <i class="fas fa-fw fa-table"></i>
		        <span>Administración de salas</span></a>
		        <ul>
		            <li class="nav-item">
		                <a class="nav-link" href="salas">
		                    <i class="fas fa-fw fa-table"></i>
		                    <span>Creación de salas</span>
		                </a>
		            </li>
		            <li class="nav-item">
		                <a class="nav-link" href="agenda">
		                    <i class="fas fa-fw fa-table"></i>
		                    <span>Creación de agendas</span>
		                </a>
		            </li>
		            <li class="nav-item">
		                <a class="nav-link" href="reserva_sala">
		                    <i class="fas fa-fw fa-table"></i>
		                    <span>Reserva de salas</span>
		                </a>
		            </li>
		        </ul>
		    </li>
		  </ul>


		  <div id="content-wrapper">

		    <div class="container-fluid">

		      <!-- Breadcrumbs-->

		      <!-- Page Content -->
		      @yield('content')

		    </div>

		    <!-- Scroll to Top Button-->
		    <a class="scroll-to-top rounded" href="#page-top">
		      <i class="fas fa-angle-up"></i>
		    </a>
		    <!-- /.container-fluid -->

		    <!-- Sticky Footer -->
		    <footer class="sticky-footer">
		      <div class="container my-auto">
		        <div class="copyright text-center my-auto">
		          <span>Copyright © Portal de reservas de Micros</span>
		        </div>
		      </div>
		    </footer>

		  </div>
		  <!-- /.content-wrapper -->

		</div>

		@yield('scripts')
	</body>
</html>