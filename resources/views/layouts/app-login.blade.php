<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  	<meta name="description" content="">
	  	<meta name="author" content="">

		<title></title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/datepicker.css" rel="stylesheet">
		<link href="css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="css/timepicki.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

		<script src="js/jquery-easing/jquery.easing.min.js"></script>
		<script src="js/jquery/timepicki.js"></script>

		<script src="js/sb-admin.min.js"></script>

		<!-- Custom styles for this template-->
		<link href="css/sb-admin.css" rel="stylesheet">

		<title>Micro reservas - @yield('title')</title>
	</head>
	<body>
		<nav class="navbar navbar-expand navbar-dark static-top" style="background-color: #1c734c">
		  <a class="navbar-brand mr-1" href="inicio">Portal de reservas de Micros</a>
		</nav>

		<div id="wrapper">
		  <div id="content-wrapper">

		    <div class="container-fluid">

		      <!-- Breadcrumbs-->

		      <!-- Page Content -->
		      @yield('content')

		    </div>

		    <!-- Scroll to Top Button-->
		    <a class="scroll-to-top rounded" href="#page-top">
		      <i class="fas fa-angle-up"></i>
		    </a>
		    <!-- /.container-fluid -->

		  </div>
		  <!-- /.content-wrapper -->

		</div>

		@yield('scripts')
	</body>
</html>